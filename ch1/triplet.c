#include "common.h"
#include "triplet.h"

Status InitTriplet(Triplet *pT, ElemType v1, ElemType v2, ElemType v3)
{
		if ((*pT = (Triplet)malloc(3*sizeof(ElemType))) == NULL)
				exit(OVERFLOW);

		(*pT)[0] = v1;
		(*pT)[1] = v2;
		(*pT)[2] = v3;

		return OK;
}

Status DestroyTriplet(Triplet *pT)
{
		free(*pT);
		*pT = NULL;
		return OK;
}

Status Get(Triplet T, int i, ElemType *e)
{
		if (T == NULL)
				return ERROR;

		if (i < 1 || i > 3)
				return ERROR;

		*e = T[i-1];
		return OK;
}

Status Put(Triplet *pT, int i, ElemType e)
{
		if (*pT == NULL)
				return ERROR;

		if (i < 1 || i > 3)
				return ERROR;

		(*pT)[i-1] = e;
		return OK;
}

Status IsAscending(Triplet T)
{
		if (T == NULL)
				return ERROR;
		if (T[0] <= T[1] && T[1] <= T[2])
				return TRUE;
		return FALSE;
}

Status IsDescending(Triplet T)
{
		if (T == NULL)
				return ERROR;
		if (T[0] >= T[1] && T[1] >= T[2])
				return TRUE;
		return FALSE;
}

Status Max(Triplet T, ElemType *e)
{
		if (T == NULL)	
				return ERROR;
		ElemType tmp = T[0];
		if (tmp <= T[1])
				tmp = T[1];
		if (tmp <= T[2])
				tmp = T[2];
		*e = tmp;
		return OK;
}

Status Min(Triplet T, ElemType *e)
{
		if (T == NULL)	
				return ERROR;
		ElemType tmp = T[0];
		if (tmp >= T[1])
				tmp = T[1];
		if (tmp >= T[2])
				tmp = T[2];
		*e = tmp;
		return OK;
}
