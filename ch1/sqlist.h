/*
 * 线性表的顺序实现
 */
#ifndef _SQLIST_H
#define _SQLIST_H
#include "common.h"

typedef int ElemType;

#define LIST_INIT_SIZE 6		/* 线性表存储空间的初始分配量 */
#define LISTINCREMENT  2		/* 线性表存储空间的分配增量 */

typedef struct
{
		ElemType *elem;		/* 存储空间基地址 */
		int length;			/* 当前长度 */
		int listsize;		/* 当前分配的存储容量（以 sizeof(ElemType)为单位） */
}SqList;

/* 创建空线性表 */
Status InitList_Sq(SqList *pL);

/*
 * 初始条件：L 存在
 * 销毁线性表 L
 */
void DestroyList_Sq(SqList *pL);

/*
 * 初始条件：线性表 L 存在
 * 操作结果：将 L 置空，即 length = 0
 */
void ClearList_Sq(SqList *pL);

/*
 * 初始条件：L 存在
 * 操作结果：若 L 为空表，则返回 TRUE，否则返回 FALSE
 */
Boolean ListEmpty_Sq(SqList L);

/*
 * 初始条件：L 存在
 * 操作结果：返回 L 中的数据元素的个数
 */
int ListLength_Sq(SqList L);

/*
 * 初始条件：L 存在，1 <= i <= ListLength_Sq(L)
 * 操作结果：用 e 接收 L 中返回的第 i 个元素的值
 */
Status GetElem_Sq(SqList L, int i, ElemType *e);

/*
 * 初始条件：L 存在
 * 操作结果：若 cur_e 是 L 的数据元素，且不是第一个，则用 pre_e 接收返回它的前驱，否则
 *			操作失败，pre_e 未定义
 */
Status PrioElem_Sq(SqList L, ElemType cur_e, ElemType *pre_e);

/*
 * 初始条件：L 存在
 * 操作结果：若 cur_e 是 L 的数据元素，且不是最后一个，则用 next_e 接收返回它的后继，否则
 *			操作失败，next_e 未定义
 */
Status NextElem_Sq(SqList L, ElemType cur_e, ElemType *next_e);

/*
 * 初始条件：L 存在，1 <= i <= ListLength_Sq(L) + 1
 * 操作结果：在 L 中第 i 个元素之前插入新的数据元素 e, L 的长度加1
 */
void ListInsert_Sq(SqList *pL, int i, ElemType e);

/*
 * 初始条件：L 存在且非空，1 <= i <= ListLength_Sq(L)
 * 操作结果：删除 L 中的第 i 个元素，并用 e 接收其删除的值，L 的长度减1
 */
void ListDelete_Sq(SqList *pL, int i, ElemType *e);


#endif /* _SQLIST_H */
