#include "sqlist.h"

Status InitList_Sq(SqList *pL)
{
		if ((pL->elem = (ElemType *)malloc(LIST_INIT_SIZE*sizeof(ElemType))) == NULL)
				exit(OVERFLOW);

		pL->length = 0;
		pL->listsize = LIST_INIT_SIZE;
		return OK;
}

void DestroyList_Sq(SqList *pL)
{
		free(pL->elem);
		pL->elem = NULL;
		pL->length = 0;
		pL->listsize = 0;
}

void ClearList_Sq(SqList *pL)
{
		pL->length = 0;
}

Boolean ListEmpty_Sq(SqList L)
{
		return L.length;
}

int ListLength_Sq(SqList L)
{
		return L.length;
}

Status GetElem_Sq(SqList L, int i, ElemType *e)
{
		if (i < 1 || i > L.length)
				return ERROR;
		*e = L.elem[i-1];
		return OK;
}

Status PrioElem_Sq(SqList L, ElemType cur_e, ElemType *pre_e)
{
		if (L.length <= 1 || L.elem == NULL)
				return ERROR;

		int lastIndex = L.length - 1;
		for (int i = 1; i < lastIndex; i++)
		{
				if (cur_e == L.elem[i])
				{
						*pre_e = L.elem[i-1];
						return OK;
				}
		}
		
		return ERROR;
}

Status NextElem_Sq(SqList L, ElemType cur_e, ElemType *next_e)
{
		if (L.length <= 1 || L.elem == NULL)
				return ERROR;

		int last2Index = L.length - 2;
		for (int i = 0; i < last2Index; i++)
		{
				if (cur_e == L.elem[i])
				{
						*next_e = L.elem[i+1];
						return OK;
				}
		}

		return ERROR;
}

void ListInsert_Sq(SqList *pL, int i, ElemType e)
{
		if (i < 1 || i > pL->length + 1)
				return;

		if (pL->length == pL->listsize)
		{
				ElemType *newelem = (ElemType *)realloc(pL->elem,
								(pL->listsize + LISTINCREMENT)*sizeof(ElemType));
				if (newelem == NULL)
						return;
				pL->elem = newelem;
				pL->listsize += LISTINCREMENT;
		}

		/*
		* 从位置 i 开始，一次向后移动元素 *
		int moveCount = pL->length - i + 1;
		for (int j = 1; j <= moveCount; j++)
		{
				pL->elem[pL->length - j + 1] = pL->elem[pL->length - j];
		}
		*/
		ElemType *p = &(pL->elem[i-1]);
		for (ElemType *q = &(pL->elem[pL->length-1]); q >= p; q--)
				*(q+1) = *q;

		pL->elem[i - 1] = e;
		pL->length++;
}

void ListDelete_Sq(SqList *pL, int i, ElemType *e)
{
		if (i < 1 || i > pL->length || pL->elem == NULL)
				return;

		*e = pL->elem[i-1];
		/*
		int moveCount = pL->length - i;
		for (int j = 1; j <= moveCount; j++, i++ )
				pL->elem[i-1] = pL->elem[i];
		*/
		ElemType *p = &(pL->elem[i - 1]);
		ElemType *q = &(pL->elem[pL->length - 1]);
		for (; p < q; p++)
				*p = *(p+1);


		pL->length--;
}
