
#include "common.h"
#include "triplet.h"


int main()
{
		Triplet T;
		InitTriplet(&T, 2018, 5, 3);
		printf("T已被初始化，T的地址为: %p\n", T);
		printf("T的初始值为：%d, %d, %d\n", T[0], T[1], T[2]);

		return 0;
}
