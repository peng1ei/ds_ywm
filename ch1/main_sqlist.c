#include "sqlist.h"

void printSqList(SqList L)
{		
		for (int i = 0; i < L.listsize; i++)
		{
				if (i < L.length)
				{
						printf("\033[0;37m[\033[0m %3d \033[0;37m]\033[0m", L.elem[i]);
						continue;
				}

				printf("\033[0;37m[\033[0m     \033[0;37m]\033[0m");
		}

		printf("\n");
}

int main()
{
		SqList L;
		InitList_Sq(&L);
		printf("SqList elem = %p, length = %d, listsize = %d\n", L.elem, L.length, L.listsize);

		ListInsert_Sq(&L, 1, 3);
		ListInsert_Sq(&L, 2, 4);
		ListInsert_Sq(&L, 3, 2);
		ListInsert_Sq(&L, 4, 1);
		ListInsert_Sq(&L, 5, 7);

		printf("Init elem: \n");
	    printSqList(L);	

		printf("Insert elem 0 at location 2: \n");
		ListInsert_Sq(&L, 2, 0);
		printSqList(L);

		printf("Insert elem 66 at location 7: \n");
		ListInsert_Sq(&L, 7, 66);
		printSqList(L);

		printf("Delete first elem: \n");
		ElemType e;
		ListDelete_Sq(&L, 1, &e);
		printf("Have deleted elem e = %d\n", e);
		printSqList(L);

		return 0;
}
