/*
 * common.h 文件
 * 包含一些常用头文件及预定义宏, 基本上后续的每章都会使用该文件
 */
#include <string.h>
#include <ctype.h>
#include <malloc.h>
#include <limits.h>		/* INT_MAX等*/
#include <stdio.h>		/* EOF(=^Z or F6), NULL */
#include <stdlib.h>
/*#include <io.h>			 eof() */
#include <math.h>
/*#include <process.h>	 exit() */

/*
 * 函数结果状态码
 */
#define TRUE 1
#define FALSE 0
#define OK 1
#define ERROR 0
#define INFEASIBLE -1
/* #define OVERFLOW -2 因为在 math.h 中已经定义 OVERFLOW 值为3， 故注释去 */
typedef int Status; 	/* Status 是函数的类型，其值是函数结果状态代码，如 OK 等*/
typedef int Boolean;	/* Boolean 是布尔类型，其值是 TRUE 或 FALSE */
