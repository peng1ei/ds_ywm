/*
 * 抽象数据类型三元组的定义
 * 数据对象：D = {e1, e2, e3 | e1, e2, e3 属于 ElemSet}
 * 数据关系：R1 = {<e1, e2>, <e2, e3>}
 */
typedef int ElemType;
typedef ElemType *Triplet;	/* 由 InitTriplet 分配 3 个元素存储空间 */

/* 基本操作集合 */

/*
 * 构造三元组 T，元素 e1, e2, e3 被分别赋予参数 v1, v2和v3 的值
 */
Status InitTriplet(Triplet *pT, ElemType v1, ElemType v2, ElemType v3);

/*
 * 销毁三元组 T，并释放空间
 */
Status DestroyTriplet(Triplet *pT);

/*
 * 返回 T 中的第 i 个元素的值，并存储在 e 中
 * 初始条件：三元组 T 已经存在，且 1 <= i <= 3
 */
Status Get(Triplet T, int i, ElemType *e);

/*
 * 将 T 中第 i 个元素的值修改为 e
 */
Status Put(Triplet *pT, int i, ElemType e);

/*
 * 判断 T 中的元素是否按升序排列， 是返回 1，否则返回0
 * 初始条件，T 已经存在
 */
Status IsAscending(Triplet T);
/*
 * 判断 T 中的元素是否按降序排列， 是返回 1，否则返回0
 * 初始条件，T 已经存在
 */
Status IsDescending(Triplet T);

/*
 * 返回 T 中最大的元素，并用 e 接收
 * 初始条件：T 存在
 */
Status Max(Triplet T, ElemType *e);


/*
 * 返回 T 中最小的元素，并用 e 接收
 * 初始条件：T 存在
 */
Status Min(Triplet T, ElemType *e);







